/*

    fetch()
        - is a method in JavaScript, which allows to send request  to an API and process its response
        - 2 arguments
            - the url to resource/route
            - optional object which contains additional information about our requests such as method, the body, and the headers of our request

    Syntax:
        fetch(url, options)

*/

// Get post data using fetch()
fetch('https://jsonplaceholder.typicode.com/posts')
    .then((response) => response.json())
    .then((data) => {
        showPosts(data)
        // console.log(data[0])
    });

// window.addEventListener('load', (e) => {
//     showPosts();
// });

// Add Post Data
document.querySelector("#form-add-post")
    .addEventListener("submit", (e) => {

        e.preventDefault();

        fetch('https://jsonplaceholder.typicode.com/posts', {
            method: 'POST',
            body: JSON.stringify({
                title: document.querySelector('#txt-title').value,
                body: document.querySelector('#txt-body').value,
                userId: 1
            }),
            headers: {
                'Content-type': 'application/json; charset=UTF-8'
            }
        })
        .then((response) => response.json())
        .then((data) => {
            console.log(data);
            alert('Successfully added!');

            // Clear or reset the input fields upon post creation
            document.querySelector('#txt-title').value = null;
            document.querySelector('#txt-body').value = null;

        });
    });

const showPosts = (posts) => {

    // let posts = await fetch('https://jsonplaceholder.typicode.com/posts').then((result, error) => {
    //     if (error) {
    //         return false;
    //     }
    //     else {
    //         return result.json();
    //     }
    // });

    // if (posts === false) {
    //     document.querySelector('#div-post-entries').innerHTML = 'Nothing to display';
    // }

    // else {
    //     let postEntries = '';

    //     posts.forEach((post) => {

    //         postEntries += `
    //             <div id="post-${post.id}">
    //                 <h3 id="post-title-${post.id}">${post.title}</h3>
    //                 <p id="post-body-${post.id}">${post.body}</p>
    //                 <button onclick="editPost('${post.id}')">Edit</button>
    //                 <button onclick="deletePost('${post.id}')">Delete</button>
    //             </div>
    //         `
    //     })

    //     document.querySelector('#div-post-entries').innerHTML = postEntries;
    // }
    let postEntries = '';

    posts.forEach((post) => {

        postEntries += `
            <div id="post-${post.id}">
                <h3 id="post-title-${post.id}">${post.title}</h3>
                <p id="post-body-${post.id}">${post.body}</p>
                <button onclick="editPost('${post.id}')">Edit</button>
                <button onclick="deletePost('${post.id}')">Delete</button>
            </div>
        `
    })

    document.querySelector('#div-post-entries').innerHTML = postEntries;
};

// Edit Post
const editPost = (id) => {

    let title = document.querySelector(`#post-title-${id}`).innerHTML;
    let body = document.querySelector(`#post-body-${id}`).innerHTML;

    document.querySelector('#txt-edit-id').value = id;
    document.querySelector('#txt-edit-title').value = title;
    document.querySelector('#txt-edit-body').value = body;

    // Removes the disabled attribute from the button
    document.querySelector('#btn-submit-update').removeAttribute('disabled')

};

document.querySelector("#form-edit-post")
    .addEventListener('submit', (e) => {

        e.preventDefault();

        fetch('https://jsonplaceholder.typicode.com/posts/1', {
            method: 'PUT',
            body: JSON.stringify({
                id: document.querySelector("#txt-edit-id").value,
                title: document.querySelector('#txt-edit-title').value,
                body: document.querySelector('#txt-edit-body').value,
                userId: 1
            }),
            headers: {
                'Content-type': 'application/json; charset=UTF-8'
            }
        })
        .then((response) => response.json())
        .then((data) => {
            console.log(data)
            alert('Successfully Updated!')

            document.querySelector("#txt-edit-id").value = null
            document.querySelector("#txt-edit-title").value = null
            document.querySelector("#txt-edit-body").value = null

            document.querySelector("#btn-submit-update")
                .setAttribute('disabled', true);
        })
    });

    /*

    Mini Activity
        - retrieve a single post, and print it in the console

    */

const deletePost = (id) => {

    // Console ko lang yung de-delete haha
    console.log({
        msg: 'Deleted Post',
        body: {
            title: document.querySelector(`#post-title-${id}`).innerHTML,
            body: document.querySelector(`#post-body-${id}`).innerHTML
        }
    });

    // Delete in the frontend
    document.querySelector(`#post-${id}`).remove();

    // Delete in the database
    fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
        method: 'DELETE'
    }).then((response) => console.log(response.status))
}
